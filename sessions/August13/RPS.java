import java.util.Scanner;
import java.util.Random;

public class RPS {

	static Scanner in;

	static int userWins, cpuWins, ties;
	
	public static void main(String[] args) {
		//play the game
		in = new Scanner(System.in);
		
		do {
			
			playRound();
			
			
		} while(playAgain());
	}
	
	public static boolean playAgain() {
		//ask the user if they want to play again
		//(return true if they do, false otherwise)
		
		System.out.print("Do you wish to play again? (y/n): ");
		String input = in.nextLine();
		
		return input.startsWith("y") || input.startsWith("Y");
	}

	public static void playRound() {
		//play a round of rock-paper-scissors
		
		int whoWon = testWinner(getUserInput(), generateCPUInput());
		
		
		if(whoWon == 0) {
			ties++;
			System.out.println("It was a tie!");
		} else if(whoWon == 1) {
			userWins++;
			System.out.println("You won!");
		} else if(whoWon == 2) {
			cpuWins++;
			System.out.println("You lost!");
		}
		
		printScores();
	}

	public static void printScores() {
		//print the scores so far (wins, ties, losses)
		
		System.out.println("You have " + userWins + " wins, " + cpuWins + " losses, and " + ties + " ties!");
		
	}
	
	
	// UTILITY METHODS
	// methods that are not part of the core game cycle but simplify code
	// should be placed below

	public static String getUserInput() {
		System.out.print("What would you like to play (ROCK, PAPER, SCISSORS): ");
		String input = in.nextLine();
		
		if(input.equalsIgnoreCase("ROCK") || input.equalsIgnoreCase("PAPER") || input.equalsIgnoreCase("SCISSORS")) {
			return input;
		} else {
			System.out.println("INVALID INPUT, MUST BE ROCK, PAPER, OR SCISSORS!");
			return getUserInput();
		}
	}
	
	public static String generateCPUInput() {
		Random generator = new Random();
		int num = generator.nextInt(3);
		
		if(num==0) {
			return "ROCK";
		} else if(num==1) {
			return "PAPER";
		} else {
			return "SCISSORS";
		}
	}
	
	public static int testWinner(String userHand, String cpuHand) {
		
		if(userHand.equalsIgnoreCase(cpuHand)) {
			return 0;
		} else if(userHand.equalsIgnoreCase("ROCK") && cpuHand.equalsIgnoreCase("PAPER")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("PAPER") && cpuHand.equalsIgnoreCase("ROCK")) {
			return 1;
		} else if(userHand.equalsIgnoreCase("ROCK") && cpuHand.equalsIgnoreCase("SCISSORS")) {
			return 1;
		} else if(userHand.equalsIgnoreCase("SCISSORS") && cpuHand.equalsIgnoreCase("ROCK")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("PAPER") && cpuHand.equalsIgnoreCase("SCISSORS")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("SCISSORS") && cpuHand.equalsIgnoreCase("PAPER")) {
			return 1;
		} else {
			System.out.println("INVALID TESTWINNER CALL - immpossible");
			return -1;
		}
	}
}