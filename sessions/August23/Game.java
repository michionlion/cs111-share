public class Game {

	Player human;
	Player ai;

	public Game() {
		//set up players
		
		human = new Human();
		ai = new Human();
	}

	public void start() {
	
		Round currentRound;
		int rounds = 0;
		
		
		do {
			currentRound = new Round(human, ai);
			currentRound.playRound();
			rounds++;
			
			if(currentRound.getTie()) {
				System.out.println("It was a tie");
			} else {
				Player p = currentRound.getWinner();
				p.won();
				System.out.println(p.getWinsDesc());
			}
			
		} while(human.readyForNextRound() && ai.readyForNextRound());
		
		System.out.println("Thank you for playing!\nYou have played " + rounds + " rounds.");
		System.out.println(human.getWinsDesc());
		System.out.println(ai.getWinsDesc());
	
	}

	
	public static void main(String[] args) {
		Game g = new Game();
		g.start();
	}

}