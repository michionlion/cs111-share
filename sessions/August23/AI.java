import java.util.Random;
public class AI extends Player {
	
	public boolean readyForNextRound() {
		return true;
	}
	
	public String getHand() {
		Random generator = new Random();
		int num = generator.nextInt(3);
		
		if(num==0) {
			return "ROCK";
		} else if(num==1) {
			return "PAPER";
		} else {
			return "SCISSORS";
		}
	}
	
	public String desc() {
		return "The AI";
	}
	
	@Override
	public String toString() {
		return "An AI object with " + wins + " wins so far";
	}
}