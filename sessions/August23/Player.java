public abstract class Player {
	
	protected int wins;
	
	public Player() {
		wins = 0;
	}
	
	public void won() {
		wins++;
		System.out.println(desc() + " won!");
	}
	
	public String getWinsDesc() {
		return desc() + " has won " + wins + " times!";
	}
	
	public abstract String desc();
	public abstract boolean readyForNextRound();
	public abstract String getHand();	
}