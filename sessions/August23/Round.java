public class Round {

	private int winner;
	private Player p1, p2;

	public Round(Player p1, Player p2) {
		this.p1 = p1;
		this.p2 = p2;
		
		winner = -1;
	}
	
	public void playRound() {
		//play a round of rock-paper-scissors

		int whoWon = testWinner(p1.getHand(), p2.getHand());
		
		if(whoWon == 0) {
			winner = 0;
		} else if(whoWon == 1) {
			winner = 1;
		} else if(whoWon == 2) {
			winner = 2;
		}
	}
	
	public Player getWinner() {
		if(winner==1) {
			return p1;
		} else if(winner == 2) {
			return p2;
		} else {
			return null;
		}
	}
	
	public boolean getTie() {
		return winner == 0;
	}

	private int testWinner(String userHand, String cpuHand) {
		
		if(userHand.equalsIgnoreCase(cpuHand)) {
			return 0;
		} else if(userHand.equalsIgnoreCase("ROCK") && cpuHand.equalsIgnoreCase("PAPER")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("PAPER") && cpuHand.equalsIgnoreCase("ROCK")) {
			return 1;
		} else if(userHand.equalsIgnoreCase("ROCK") && cpuHand.equalsIgnoreCase("SCISSORS")) {
			return 1;
		} else if(userHand.equalsIgnoreCase("SCISSORS") && cpuHand.equalsIgnoreCase("ROCK")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("PAPER") && cpuHand.equalsIgnoreCase("SCISSORS")) {
			return 2;
		} else if(userHand.equalsIgnoreCase("SCISSORS") && cpuHand.equalsIgnoreCase("PAPER")) {
			return 1;
		} else {
			System.out.println("INVALID TESTWINNER CALL - immpossible");
			return -1;
		}
	}
}