import java.util.Scanner;

public class Human extends Player {
	
	public boolean readyForNextRound() {
		System.out.println("Do you want to continue? (y/n)");
		Scanner scan = new Scanner(System.in);
		
		String input = scan.nextLine();
		
		return input.startsWith("y") || input.startsWith("Y");
	}
	
	public String getHand() {
		//prompt the user for a hand to play
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("What would you like to play (ROCK, PAPER, SCISSORS): ");
		String input = in.nextLine();
		
		if(input.equalsIgnoreCase("ROCK") || input.equalsIgnoreCase("PAPER") || input.equalsIgnoreCase("SCISSORS")) {
			return input;
		} else {
			System.out.println("INVALID INPUT, MUST BE ROCK, PAPER, OR SCISSORS!");
			return getHand();
		}
	}
	
	public String desc() {
		return "You";
	}
	
	public void test() {
		System.out.println("test");
	}
	
	@Override
	public String toString() {
		return "A Human object with " + wins + " wins so far";
	}
}