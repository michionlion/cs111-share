public class RepetitionUsingWhile {


	public static void main(String[] args) {
		
		//the amount of times we want to repeat - we could also just use a constant in the while assertion
		int repeatTimes = 10;
		
		//the counter, which we will use to count how many times we have repeated
		int counter = 0;
		
		//the while loop, checking if we have repeated enough times
		while(counter < repeatTimes) {
			
			//DO STUFF
			//debug, print out counter to ensure the loop is repeating
			System.out.println(counter);
			
			//increment counter
			counter = counter + 1;
		}
		
	}

}