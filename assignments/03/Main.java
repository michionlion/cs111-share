import java.util.Date;

public class Main
{
    public static void main(String[] args)
    {
		//attribute yourself
        //System.out.println("Saejin Mahlau-Heinert\n" + new Date() + "\n");

        // Variables:
        Octopus ocky;           // an octopus
        Utensil spat;           // a kitchen utensil

        spat = new Utensil("spatula"); // create a utensil with the name spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        ocky = new Octopus("Ocky");    // create and name the octopus
        ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight() + " pounds");
		System.out.print("and is " + ocky.getAge() + " years old. ");
		System.out.println("His favorite utensil is a " + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $" + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + spat.getColor());

        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());
    }
}
